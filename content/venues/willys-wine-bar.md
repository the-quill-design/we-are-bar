---
title: Willy's Wine Bar
logo: /img/willyslogo.png
header: /img/20190531092124-ph-cfaruolo-scaled.jpg
exposition:
  description: >-
    # Willy's Wine Bar

    Just a stones throw from Fenchurch Street Station, Willy's Wine Bar is one of the most established and popular wine bars in the square mile. Towards the back of the bar is the restaurant which serves an extensive English menu, together with a range of daily specials.


    <button onclick="document.querySelector('#book').classList.add('active');">Book</button>

menus:
  image: /img/20190531102056-ph-cfaruolo-scaled.jpg
  menus:
    - name: Restaurant Menu
      file: /menus/willys/Restaurant%20Menu.pdf
    - name: Bar Menu
      file: /menus/willys/Bar%20Menu.pdf
    - name: Drinks Menu
      file: /menus/willys/Drinks%20Menu.pdf
    - name: Classic Cocktail Menu
      file: /menus/willys/Cocktails.pdf

christmas:
  link: /menus/willys/Christmas.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Whether it's a drinks reception or a traditional Christmas dinner you're looking for, we've got festive packages to suit all parties. 


    Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!


contact:
  address:
    streetAddress: Willy’s Wine Bar
    addressLocality: 107 Fenchurch St
    addressRegion: London
    postalCode: EC3M 5JF
  phone: 0204 5818 400
  email: willys@wearebar.com
  aside: <div class="responsive-container"><iframe
    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9932.144137463929!2d-0.0792361!3d51.5125549!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbab8af49c79a75c5!2sWillys%20Wine%20Bar!5e0!3m2!1sen!2suk!4v1630572605485!5m2!1sen!2suk"
    width="600" height="450" style="border:0;" allowfullscreen=""
    loading="lazy"></iframe></div>
  opening_hours:
    - day: Mon to Fri
      from: 12pm
      to: 11pm
    - day: Saturday & Sunday
      from: " "
      to: Available for hire
booking: <script type='text/javascript'
  src='//www.opentable.co.uk/widget/reservation/loader?rid=94944&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
social:
  facebook: https://www.facebook.com/willysfenchurch
  instagram: https://www.instagram.com/willysfenchurch/
gallery:
  - image: /img/w1.jpg
    name: Willy's Wine Bar
  - name: Willy's Wine Bar
    image: /img/w2.jpg
  - name: Willy's Wine Bar
    image: /img/w3.jpg
  - name: Willy's Wine Bar
    image: /img/w4.jpg
  - name: Willy's Wine Bar
    image: /img/w5.jpg
extra:
  - title: WHAT'S ON
    image: /img/willys/tabletalker.jpg
    description: >-
      <a href="/menus/willys/Table%20Talker.pdf" title="Events Guide" class="button">Download our What's On guide</a>
  - description: >-
      If you want to host a private party or a corporate event, all our wine bars are available for your exclusive hire. We can tailor everything so that when things get underway it’s all done your way. And whatever the occasion, we have the drinks and the service to make it truly remarkable.
    title: EVENTS, PRIVATE PARTIES, MEETINGS
    image: /img/w1.jpg
  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      Our luxurious private dining room is perfect for those all important meetings, casual seated meals or small intimate gatherings.
    image: /img/willysprivatespace.jpg
  - description: >-
      Seated |  50 people

      Standing | 80 people


      Our bar area is a versatile space that is perfect for an after work drink, a private party, quiz night or to enjoy a full sit-down three course lunch or dinner.


      Enjoy a drinks reception with friends or colleagues at the private bar, it has all the party space you need!
    title: The Restaurant
    image: /img/willystherestaurant.jpg
  - title: Full Venue
    image: /img/willysfullvenue.jpg
    description: >-
      Seated |  60 people

      Standing | 120 people


      Our bar area is a versatile space that is perfect for an after work drink, a private party, quiz night or to enjoy a full sit-down three course lunch or dinner.


      Enjoy a drinks reception with friends or colleagues at the private bar, it has all the party space you need!
---
