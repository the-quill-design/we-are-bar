---
title: Number 25
logo: /img/number25-logo.png
header: /img/number25-1.jpg
exposition:
  description: >-
    # Number 25

    A City staple for after work drinks and British style dining, Number 25 is a top destination for drinks in the heart of London; Whether that's a delicious bottle of wine, or the lapping up of a cold pint.


    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>
  background: /img/5e51033b258ffe422581b471_the-bolthole-background.jpg
menus:
  image: /img/number25-food.jpg
  menus:
    - name: Food Menu
      file: /menus/number25/Food_Menu.pdf
    - name: Drinks Menu
      file: /img/number25-drinks.pdf
    - name: Platter Menu
      file: /menus/number25/Platter_Menu.pdf
    - name: Bar Menu
      file: /menus/number25/Bar_Snacks.pdf

christmas:
  link: /menus/Christmas 1.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Perfect for drinks parties and large get togethers. Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - name: Number 25
    image: /img/number25-1.jpg
  - name: Number 25
    image: /img/number25-2.jpg
  - name: Number 25
    image: /img/number25-3.jpg
  - name: Number 25
    image: /img/number25-4.jpg

extra:
  - title: WHAT'S ON
    image: /img/number25/tabletalker.jpg
    description: >-
      <a href="/menus/number25/Table%20Talker.pdf" title="Events Guide" class="button">Download our What's On guide</a>
  - title: EVENTS, PRIVATE PARTIES, MEETINGS
    description: >-
      If you want to host a private party or a corporate event, all our wine bars are available for your exclusive hire. We can tailor everything so that when things get underway it’s all done your way. And whatever the occasion, we have the drinks and the service to make it truly remarkable.
    image: /img/number25-p1.jpg
  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      Our luxurious private dining room is perfect for those all important meetings, casual seated meals or small intimate gatherings.
    image: /img/number25-p2.jpg
  - title: Full venue
    description: >-
      Seated | 30 people

      Standing | 80 people


      This 80 capacity venue may be small, but this quirky bar is the perfect little gem for your drinks in the city. Full with the buzz of the city and some of the friendliest bar staff you’ll find Number 25 oozes charm.
    image: /img/number25-p3.jpg

contact:
  address:
    addressLocality: 25 Birchin Lane
    streetAddress: Number 25
    addressRegion: London
    postalCode: "EC3V 9DJ"
  phone: 020 7623 2505
  email: number25@wearebar.com
  aside: >-
    <div class="responsive-container">

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.004290598182!2d-0.08835388422983086!3d51.513137279636126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487603530e0029bf%3A0xfaaf4c75c6dfb0c0!2sNumber%2025!5e0!3m2!1sen!2suk!4v1630913016534!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

    </div>
  opening_hours:
    - day: Mon to Fri
      from: 11am
      to: 11pm
    - day: Sat / Sun
      from: " "
      to: Available for hire
social:
  facebook: https://www.facebook.com/number25winebar/
  instagram: https://www.instagram.com/number25winebar/
date: 2021-08-24T10:46:23.317Z
booking: <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=259377&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
---
blah
