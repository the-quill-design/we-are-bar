---
title: St Mary at Hill
logo: /img/logo.png
header: /img/stmarys/main.jpg
exposition:
  description: >-
    # St Mary at Hill

    Amongst the hustle and bustle of London’s city folk sits Jamie's St Mary at Hill, where indulging in long lunches is custom and wine is enjoyed at a leisurely pace.


    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>

menus:
  image: /img/stmarys-food.jpg
  menus:
    - name: Food Menu
      file: /menus/stmary/Food%20Menu.pdf
    - name: Bar Food Menu
      file: /menus/stmary/Bar%20Food%20Menu.pdf
    - name: Drinks Menu
      file: /menus/stmary/Drinks%20Menu.pdf
    - name: Lunch Offer
      file: /menus/stmary/Lunch%20Offer.pdf
    - name: Classic Cocktail Menu
      file: /menus/stmary/Cocktails.pdf

christmas:
  link: /menus/Christmas 2.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Whether it's a drinks reception or a traditional Christmas dinner you're looking for, we've got festive packages to suit all parties. 


    Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - name: St Mary at Hill
    image: /img/stmarys1.jpg
  - name: St Mary at Hill
    image: /img/stmarys2.jpg
  - name: St Mary at Hill
    image: /img/stmarys3.jpg
  - name: St Mary at Hill
    image: /img/stmarys4.jpg

extra:
  - title: EVENTS, PRIVATE PARTIES
    description: >-
      At Jamies, we don’t just pride ourselves on our knowledge of fine wine, spirits and beer. We are equally adept at organising events where you can enjoy drinking them.If you want to host a private party or a corporate event, all our wine bars are available for your exclusive hire. We can tailor everything so that when things get underway it’s all done your way. And whatever the occasion, we have the drinks and the service to make it truly remarkable.
    image: /img/stmarysp1.jpg
  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      Our luxurious private dining room is perfect for those all important meetings, casual seated meals or small intimate gatherings.
    image: /img/stmarysp2.jpg

  - title: Bar Area
    image: /img/stmarys-bar.jpg
    description: >-
      Seated | 60 people

      Standing | 120 people


      Our bar area is a versatile space that is perfect for an after work drink, a private party, quiz night or to enjoy a full sit-down three course lunch or dinner.


      Enjoy a drinks reception with friends or colleagues at the private bar, it has all the party space you need!

  - title: The Dining Room
    image: /img/stmarys-dining.jpg
    description: >-
      Seated | 60 people

      Standing | 90 people


      Our top bar and restaurant is a gorgeous area to be enjoyed by all. It provides its own private bar and it’s own wine cellar, choose from our exquisite wine range and enjoy!


      This area is ideal for lunch or dinner, a drinks reception, or hire our whole top bar for yourself.

  - title: Whisky Room
    image: /img/stmarys-whisky.jpg
    description: >-
      Seated | 17 people

      Standing | 20 people


      This semi private escape tucked away just of the main bar area is a great spot for casual gatherings of up to 20 people. Plush furnishings and a laid back setting makes the Whisky room the go to area for intimate drinks parties.

  - title: Hooke Room
    image: /img/stmarys-hooke.jpg
    description: >-
      Seated | 20 people

      Standing | 30 people


      Our private dining room can be found at the far end of the top bar. It can be enjoyed for all any occasions including birthday celebrations, leaving drinks, entertaining clients or simply a get together with friends.


contact:
  address:
    streetAddress: 2 St Mary at Hill
    addressRegion: London
    addressLocality: Billingsgate
    postalCode: "EC3R 8EE"
  phone: 020 7283 9504
  email: stmaryathill@wearebar.com
  aside: >-
    <div class="responsive-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.15614478304!2d-0.08562338422991843!3d51.510351279635564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876035221c890a7%3A0x4b69b2a202590250!2s2%20St%20Mary%20at%20Hill%2C%20London%20EC3R%208EE!5e0!3m2!1sen!2suk!4v1631001835616!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
  opening_hours:
    - day: Mon to Fri
      from: 12pm
      to: 11pm
    - day: Sat / Sun
      from: " "
      to: Available for hire
social:
  facebook: https://www.facebook.com/JamiesStMarys
  instagram: https://www.instagram.com/jamiesstmarys
date: 2021-08-24T10:46:23.317Z
booking: <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=94947&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
---
blah
