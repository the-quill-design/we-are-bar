---
title: Tudor St
logo: /img/logo.png
header: /img/tudorst1.jpg
exposition:
  description: >-
    # Tudor St

    Just a short walk from Blackfriars Station, Jamie's Tudor Street is tucked away at the end of Tudor Street. Featuring a large restaurant and bar area, you can indulge in the popular Jamie's style menu and the extensive wine list synonymous with the brand.


    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>

menus:
  image: /img/tudorst-food.jpg
  menus:
    - name: Food Menu
      file: /menus/tudorst/Food%20Menu.pdf
    - name: Drinks Menu
      file: /menus/tudorst/Drinks%20Menu.pdf
    - name: Bar Menu
      file: /menus/tudorst/Bar%20Menu.pdf
    - name: Set Menu
      file: /menus/tudorst/Set%20Menu.pdf
    - name: Classic Cocktail Menu
      file: /menus/tudorst/Cocktails.pdf

christmas:
  link: /menus/tudorst/Christmas.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Whether it's a drinks reception or a traditional Christmas dinner you're looking for, we've got festive packages to suit all parties. 


    Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - name: Tudor St
    image: /img/tudorst1.jpg
  - name: Tudor St
    image: /img/tudorst2.jpg
  - name: Tudor St
    image: /img/tudorst3.jpg
  - name: Tudor St
    image: /img/tudorst4.jpg

extra:
  - title: EVENTS, PRIVATE PARTIES
    description: >-
      Jamies Tudor St has got twice the party space – our ground floor restaurant area and quirky basement bar are both perfect for winding down after a hard day’s work. With a wide selection of cocktails, draught beer and a hand-picked global wine list, there is something for everyone at Jamies Tudor Street.
    image: /img/tudorstp1.jpg
  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      With various spaces to host your next upcoming special occasion. Our private areas for hire are perfect for those casual seated meals or small intimate gatherings.
    image: /img/tudorstp2.jpg
  - title: The Ground Floor
    description: >-
      Seated | 30 people

      Standing | 40 people


      Enjoy a drinks reception with friends or colleagues at the private bar, it has all the party space you need!
    image: /img/tudorst-groundfloor.jpg
  - title: The Basement Bar

    description: >-
      Seated | 60 people

      Standing | 120 people


      Head downstairs to the basement bar for a good old fashion knees up, complete with its own private bar to compliment. Great for drinks and platters or dining.
    image: /img/tudorst-basement.jpg

contact:
  address:
    streetAddress: Tudor Street
    addressLocality: 36 Tudor Street
    addressRegion: London
    postalCode: "EC4Y 0BH"
  phone: 0204 5818 600
  email: info@jamiestudor.com
  aside: >-
    <div class="responsive-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.0080134396817!2d-0.11034748422983048!3d51.513068979635996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604b2fedd603f%3A0xb275ed3caba3c28!2s36%20Tudor%20St%2C%20Temple%2C%20London%20EC4Y%200BH!5e0!3m2!1sen!2suk!4v1630926942462!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
  opening_hours:
    - day: Monday
      from: 11am
      to: 11pm
    - day: Tuesday
      from: 11am
      to: 11pm
    - day: Wednesday
      from: 11am
      to: Midnight
    - day: Thursday
      from: 11am
      to: 1am
    - day: Friday
      from: 11am
      to: Midnight
    - day: Saturday
      from: ""
      to: Closed
    - day: Sunday
      from: ""
      to: Closed
social:
  facebook: https://www.facebook.com/jamiestudorstreet/
  instagram: https://www.instagram.com/jamiestudorstreet/?hl=en
date: 2021-08-24T10:46:23.317Z
booking: <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=259374&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>

modal:
    title: Get your £10 Tasty Tenner!
    content: Fancy £10 to spend on your next visit to Tudor Street? Simply sign up and your £10 voucher will automatically land in your inbox!
    link: "#signup"

---
blah
