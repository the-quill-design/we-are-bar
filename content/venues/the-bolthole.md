---
title: The Bolthole
logo: /img/5e4d6f202b1811594ccefc44_the-bolthole.png
header: /img/5e559af91ffa49fd30505b40_the-bolthole.jpg
exposition:
  description: >-
    # The Bolthole

    The Bolthole is an underground haven located in the heart of the city just off Cannon Street and a short stroll from London Bridge. Hidden from public view, this city gem houses a fantastic late night cocktail bar. Featuring original brick work archways, a bar and a number of cosy areas for group get-togethers and private parties.


    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>
  background: /img/5e51033b258ffe422581b471_the-bolthole-background.jpg
menus:
  title: Drinks
  image: /img/bolthole/drinks.jpg
  menus:
    - name: Drinks Menu
      file: /menus/bolthole/Drinks%20Menu.pdf
    - name: Happy Hour Menu
      file: /menus/bolthole/Happy%20Hour.pdf

christmas:
  link: /menus/Christmas 1.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Perfect for drinks parties and large get togethers. Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - image: /img/b1.jpg
    name: The Bolthole
  - name: The Bolthole
    image: /img/b2.jpg
  - name: The Bolthole
    image: /img/b3.jpg
  - name: The Bolthole
    image: /img/b4.jpg
  - name: The Bolthole
    image: /img/b5.jpg
extra:
  - title: WHAT'S ON
    image: /img/b5.jpg
    description: >-
      <strong>2-4-1 Happy Hour</strong>

      <em>4pm - 7pm everyday</em>

      Cocktails
      
      Beers
      
      Wines
      
      Spirits & Mixer 
      

  - title: Private Hire
    description: >-
      Leave the streets of London far behind you as you venture down into The
      Bolthole Suffolk Lane. This discreet hideaway, full of cosy alcoves and
      exposed brickwork is the perfect spot for a relaxing tipple. Looking for
      the perfect location for a secret party? Look no further!


      Want to learn more? Give us a call on 0207 626 0996 or drop us an enquiry below.


      <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>


      Full Venue Hire • Birthday Celebrations • Quiz • Networking • Events • Engagements parties • Product Launches • Weddings • Wine Tastings • Cocktail Masterclasses
    image: /img/privatehire.jpg
  - description: "Capacity: 150 standing / 67 seated"
    title: FULL VENUE HIRE
    image: /img/full.jpeg
  - title: The Arches
    image: /img/arches.jpeg
    description: "Capacity: 20 seated / 25 standing"
  - image: /img/snug.jpeg
    description: "Capacity: 10 seated / 15 standing"
    title: The Snug

contact:
  address:
    streetAddress: 2a Suffolk Lane
    addressRegion: London
    postalCode: " EC4R 0AT"
    addressLocality: City of London
  phone: 0207 626 0996
  email: suffolklane@wearebar.com
  aside: >-
    <div class="responsive-container">

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.1234633803374!2d-0.09149068385506504!3d51.510950879635665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487603541464bcef%3A0xeb292dc57e5f5b95!2sThe%20Bolthole!5e0!3m2!1sen!2suk!4v1629905658783!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

    </div>
  opening_hours:
    - day: Monday
      from: "16:00"
      to: "23:00"
    - day: Tuesday
      from: "16:00"
      to: "00:30"
    - day: Wednesday
      from: "16:00"
      to: "00:30"
    - day: Thursday
      from: "16:00"
      to: "00:30"
    - day: Friday
      from: "16:00"
      to: "00:30"
social:
  facebook: https://www.facebook.com/TheBoltholeLondon/?ref=bookmarks
  instagram: https://www.instagram.com/theboltholelondon/
date: 2021-08-24T10:46:23.317Z
address:
  streetAddress: testing
booking: <script type='text/javascript'
  src='//www.opentable.co.uk/widget/reservation/loader?rid=259371&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
---
blah