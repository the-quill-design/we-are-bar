---
title: Ludgate Hill
logo: /img/logo.png
header: /img/ludgate/1.jpg
exposition:
  description: >-
    # Ludgate Hill


    An old bank turned into a Bar, Specialising in having a good time And Group bookings



    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>
menus:
  image: /img/ludgate/mezzeboard2.jpg
  menus:
    - name: A La Carte Menu
      file: /menus/ludgate/Food%20Menu.pdf
    - name: Drinks Menu
      file: /menus/ludgate/Drinks%20Menu.pdf
    - name: Platter Menu
      file: /menus/ludgate/Platter%20Menu.pdf
    - name: Classic Cocktail Menu
      file: /menus/ludgate/Cocktails.pdf
    - name: Lunch Offer
      file: /menus/ludgate/Lunch_Offer.pdf

christmas:
  link: /menus/Christmas 1.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Perfect for drinks parties and large get togethers. Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - name: Ludgate Hill
    image: /img/ludgate/1.jpg
  - name: Ludgate Hill
    image: /img/ludgate/2.jpg
  - name: Ludgate Hill
    image: /img/ludgate/3.jpg
  - name: Ludgate Hill
    image: /img/ludgate/4.jpg
  - name: Ludgate Hill
    image: /img/ludgate/5.jpg
  - name: Ludgate Hill
    image: /img/ludgate/6.jpg
  - name: Ludgate Hill
    image: /img/ludgate/7.jpg

extra:
  - title: What's On
    description: >-
      <a href="/menus/ludgate/WhatsOn.pdf" title="Events Guide" class="button">Download What's On</a>
    image: /img/ludgate/whatson.jpg
  - title: EVENTS, PRIVATE PARTIES
    description: >-
      At Jamies, we don’t just pride ourselves on our knowledge of fine wine, spirits and beer. We are equally adept at organising events where you can enjoy drinking them.If you want to host a private party or a corporate event, all our wine bars are available for your exclusive hire. We can tailor everything so that when things get underway it’s all done your way. And whatever the occasion, we have the drinks and the service to make it truly remarkable.
    image: /img/ludgatep1.jpg
  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      Kick back with drinks and platters to share amongst your friends and loved ones. Or, if you’re feeling competitive, our Club Room has a wide range of bar games such as pool and darts to keep your guests occupied.
    image: /img/ludgate/privatespace.jpg
  - title: Main Floor
    description: >-
      Seated | 60 people

      Standing | 120 people


      Enjoy a drinks reception with party platters accompanied by friends or colleagues for up to 60 guests.

    image: /img/ludgate/mainfloor.jpg
  - title: Club Room
    description: >-
      Seated | 20 people

      Standing | 35 people


      The Club room is the perfect getaway spot away from the upstairs noise and bustle. The room comes complete with a full sized pool table, playable darts and room for up to 35 guests.
    image: /img/ludgate/clubroom.jpg

contact:
  address:
    streetAddress: Ludgate Hill
    addressRegion: London
    addressLocality: 47 Ludgate Hill
    postalCode: "EC4M 7JZ"
  phone: 020 7236 1942
  email: ludgate@wearebar.com
  aside: >-
    <div class="responsive-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.9672473154988!2d-0.10472998422979972!3d51.513816879636146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604acdfb33915%3A0x947a266544d9f46d!2s47%20Ludgate%20Hill%2C%20London%20EC4M%207JU!5e0!3m2!1sen!2suk!4v1631000751118!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
  opening_hours:
    - day: Monday
      from: 12pm
      to: 11pm
    - day: Tuesday
      from: 12pm
      to: 12am
    - day: Wednesday
      from: 12pm
      to: 12am
    - day: Thursday
      from: 12pm
      to: 12am
    - day: Friday
      from: 12pm
      to: 11pm
    - day: Saturday
      from: ""
      to: Closed
    - day: Sunday
      from: ""
      to: Closed
social:
  facebook: https://en-gb.facebook.com/jamiesludgatehill/
  instagram: https://www.instagram.com/jamiesludgatehill/
date: 2021-08-24T10:46:23.317Z
booking: <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=259368&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
---
blah
