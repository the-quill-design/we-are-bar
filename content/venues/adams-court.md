---
title: Adam's Court
logo: /img/logo.png
header: /img/adams1.jpg
exposition:
  description: >-
    # Adam's Court

    Jamie's Adam's Court is nestled in a bustling courtyard just yards from Bank Station. Take in the cityscapes from our gorgeous terrace and enjoy a tipple or two at the bar.


    <button class="book" onclick="document.querySelector('#book').classList.add('active');">Book</button>

menus:
  image: /img/adams-food.jpg
  menus:
    - name: Food Menu
      file: /menus/adamscourt/Food%20Menu.pdf
    - name: Drinks Menu
      file: /menus/adamscourt/Drinks%20Menu.pdf
    - name: Wine List
      file: /menus/adamscourt/Wine%20List.pdf
    - name: Lunch Offer
      file: /menus/adamscourt/Lunch%20Offer.pdf
    - name: Classic Cocktail Menu
      file: /menus/adamscourt/Cocktails.pdf
    - name: Dessert Menu
      file: /menus/adamscourt/Dessert.pdf
    - name: Valentines Menu
      file: /menus/adamscourt/Valentines%20Menu.pdf

christmas:
  link: /menus/Christmas 2.pdf
  description: >-
    This year immerse yourself in the spirit of Christmas. Tickle your taste buds with a feast complete with all the trimmings. Party with your colleagues, friends or loved ones. 


    Whether it's a drinks reception or a traditional Christmas dinner you're looking for, we've got festive packages to suit all parties. 


    Contact our events team <a href="mailto:events@wearebar.com" title="events@wearebar.com" style="color: #fff;">events@wearebar.com</a> and we will organise your best Christmas Party ever!

gallery:
  - name: Adam's Court
    image: /img/adams1.jpg
  - name: Adam's Court
    image: /img/adams2.jpg
  - name: Adam's Court
    image: /img/adams3.jpg
  - name: Adam's Court
    image: /img/adams4.jpg

extra:

  - title: EVENTS, PRIVATE PARTIES, MEETINGS
    description: >-
      If you’re looking for something more formal, opt to dine on our Top floor, our largest open space. The room is surrounded by beautiful Art Deco windows, featuring its own unique bar, with a mixture of low and high tables; the perfect space from a small lunch to an array of large Events. Seasonal Menu’s that offer classic British cuisine with a modern twist. With a Private Dining Room which has a feature Window encompassing the room, making it light and airy and access to AV equipment. Complimentary Wi-Fi is available throughout the venue.
    image: /img/adamsp1.jpg

  - title: LOOKING FOR A PRIVATE SPACE?
    description: >-
      Our luxurious private dining room is perfect for those all important meetings, casual seated meals or small intimate gatherings.
    image: /img/adamsp2.jpg

  - title: The Copper Bar
    description: >-
      Seated | 30 people

      Standing | 50 people


      The Copper Bar is a versatile space that is perfect for an after work drink, a private party, quiz night or to enjoy a full sit-down three course lunch or dinner.


      Enjoy a drinks reception with friends or colleagues at the private bar, it has all the party space you need!
    image: /img/adams-copper.jpg

  - title: Top Floor Restaurant
    description: >-
      Seated | 90 people

      Standing | 120 people


      Our top bar and restaurant is a gorgeous area to be enjoyed by all. It provides its own private bar and it’s own wine cellar, choose from our exquisite wine range and enjoy!


      This area is ideal for lunch or dinner, a drinks reception, or hire our whole top bar for yourself.
    image: /img/adams-restaurant.jpg

  - title: Private Dining Room
    description: >-
      Seated | 30 people

      Standing | 35 people


      The private dining room can be found at the far end of the top floor. It can be enjoyed for all any occasions including birthday celebrations, leaving drinks, entertaining clients or simply a get together with friends.
    image: /img/adams-private.jpg

contact:
  address:
    streetAddress: 6 Adam's Court
    addressLocality: Old Broad Street
    addressRegion: London
    postalCode: "EC2N 1DX"
  phone: 020 7628 0808
  email: adamscourt@wearebar.com
  aside: >-
    <div class="responsive-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.9185498023508!2d-0.08686988422976458!3d51.514710279636354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760352d3dc7833%3A0xb1707c61d42f02a!2s6%20Adam&#39;s%20Ct%2C%20London%20EC2N%201DX!5e0!3m2!1sen!2suk!4v1631000212764!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
  opening_hours:
    - day: Mon to Fri
      from: 11am
      to: 11pm
    - day: Sat / Sun
      from: " "
      to: Available for hire
social:
  facebook: https://www.facebook.com/Jamies-Adams-Court-101698528381086/
  instagram: https://www.instagram.com/jamiesadamscourt/
date: 2021-08-24T10:46:23.317Z
booking: <script type='text/javascript' src='//www.opentable.co.uk/widget/reservation/loader?rid=200487&type=standard&theme=standard&iframe=true&domain=couk&lang=en-GB&newtab=false&ot_source=Restaurant%20website'></script>
---
blah
